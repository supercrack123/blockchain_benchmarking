```
Name                                         Gascost         Time (ns)     MGas/S    Gasprice for 10MGas/S    Gasprice for ECDSA eq
-----------------------------------------  ---------  ----------------  ---------  -----------------------  -----------------------
PrecompiledEcrecover/                           3000  239468              12.5278                 2394.68                 3000
PrecompiledSha256/128                            108     864             125                         8.64                   10.824
PrecompiledRipeMD/128                           1080    2155             501.16                     21.55                   26.9973
PrecompiledIdentity/128                           27      25.2          1071.43                      0.252                   0.3157
PrecompiledModExp/eip_example1                 13056   46300             281.987                   463                     580.036
PrecompiledModExp/eip_example2                 13056    9074            1438.84                     90.74                  113.677
PrecompiledModExp/nagydani-1-square              204    2652              76.9231                   26.52                   33.2236
PrecompiledModExp/nagydani-1-qube                204    3913              52.1339                   39.13                   49.0212
PrecompiledModExp/nagydani-1-pow0x10001         3276   17618             185.946                   176.18                  220.714
PrecompiledModExp/nagydani-2-square              665    4527             146.896                    45.27                   56.7132
PrecompiledModExp/nagydani-2-qube                665    6712              99.0763                   67.12                   84.0864
PrecompiledModExp/nagydani-2-pow0x10001        10649   35039             303.918                   350.39                  438.961
PrecompiledModExp/nagydani-3-square             1894    8349             226.854                    83.49                  104.594
PrecompiledModExp/nagydani-3-qube               1894   13709             138.157                   137.09                  171.743
PrecompiledModExp/nagydani-3-pow0x10001        30310   96113             315.358                   961.13                 1204.08
PrecompiledModExp/nagydani-4-square             5580   18547             300.857                   185.47                  232.353
PrecompiledModExp/nagydani-4-qube               5580   37151             150.198                   371.51                  465.419
PrecompiledModExp/nagydani-4-pow0x10001        89292  237398             376.128                  2373.98                 2974.07
PrecompiledModExp/nagydani-5-square            17868   48185             370.821                   481.85                  603.651
PrecompiledModExp/nagydani-5-qube              17868  108108             165.279                  1081.08                 1354.35
PrecompiledModExp/nagydani-5-pow0x10001       285900  732456             390.331                  7324.56                 9176.04
PrecompiledBn256Add/chfast1                      500   17463              28.632                   174.63                  218.772
PrecompiledBn256Add/chfast2                      500   16928              29.5369                  169.28                  212.07
PrecompiledBn256Add/cdetrio1                     500    1374             363.901                    13.74                   17.2132
PrecompiledBn256Add/cdetrio2                     500    1512             330.688                    15.12                   18.942
PrecompiledBn256Add/cdetrio3                     500    1393             358.938                    13.93                   17.4512
PrecompiledBn256Add/cdetrio4                     500    1516             329.815                    15.16                   18.9921
PrecompiledBn256Add/cdetrio5                     500    1506             332.005                    15.06                   18.8668
PrecompiledBn256Add/cdetrio6                     500    1920             260.417                    19.2                    24.0533
PrecompiledBn256Add/cdetrio7                     500    1785             280.112                    17.85                   22.3621
PrecompiledBn256Add/cdetrio8                     500    1843             271.297                    18.43                   23.0887
PrecompiledBn256Add/cdetrio9                     500    1824             274.123                    18.24                   22.8507
PrecompiledBn256Add/cdetrio10                    500    1737             287.853                    17.37                   21.7607
PrecompiledBn256Add/cdetrio11                    500   17783              28.1167                  177.83                  222.781
PrecompiledBn256Add/cdetrio12                    500   18024              27.7408                  180.24                  225.801
PrecompiledBn256Add/cdetrio13                    500   17231              29.0175                  172.31                  215.866
PrecompiledBn256Add/cdetrio14                    500    2729             183.217                    27.29                   34.1883
PrecompiledBn256ScalarMul/chfast1              40000  129735             308.321                  1297.35                 1625.29
PrecompiledBn256ScalarMul/chfast2              40000  125146             319.627                  1251.46                 1567.8
PrecompiledBn256ScalarMul/chfast3              40000  119744             334.046                  1197.44                 1500.13
PrecompiledBn256ScalarMul/cdetrio1             40000  137332             291.265                  1373.32                 1720.46
PrecompiledBn256ScalarMul/cdetrio6             40000  141972             281.746                  1419.72                 1778.59
PrecompiledBn256ScalarMul/cdetrio11            40000  134497             297.404                  1344.97                 1684.95
PrecompiledBn256Pairing/jeff1                 260000       4.49644e+06    57.8235                44964.4                 56330.4
PrecompiledBn256Pairing/jeff2                 260000       4.44007e+06    58.5576                44400.7                 55624.2
PrecompiledBn256Pairing/jeff3                 260000       4.53119e+06    57.3801                45311.9                 56765.7
PrecompiledBn256Pairing/jeff4                 340000       5.60982e+06    60.608                 56098.2                 70278.5
PrecompiledBn256Pairing/jeff5                 340000       6.29685e+06    53.9952                62968.5                 78885.5
PrecompiledBn256Pairing/jeff6                 260000       4.14774e+06    62.6847                41477.5                 51962
PrecompiledBn256Pairing/empty_data            100000       1.41105e+06    70.8693                14110.5                 17677.3
PrecompiledBn256Pairing/one_point             180000       2.80056e+06    64.2729                28005.6                 35084.7
PrecompiledBn256Pairing/two_point_match_2     260000       4.26197e+06    61.0047                42619.7                 53392.9
PrecompiledBn256Pairing/two_point_match_3     260000       3.99224e+06    65.1264                39922.4                 50013.8
PrecompiledBn256Pairing/two_point_match_4     260000       4.27549e+06    60.8118                42754.9                 53562.3
PrecompiledBn256Pairing/ten_point_match_1     900000       1.46359e+07    61.4926               146359                  183355
PrecompiledBn256Pairing/ten_point_match_2     900000       1.44697e+07    62.1991               144697                  181273
PrecompiledBn256Pairing/ten_point_match_3     260000       3.92568e+06    66.2306                39256.8                 49180
```

Columns

* `MGas/S` - Shows what MGas per second was measured on that machine at that time
* `Gasprice for 10MGas/S` shows what the gasprice should have been, in order to reach 10 MGas/second
* `Gasprice for ECDSA eq` shows what the gasprice should have been, in order to have the same cost/cycle as ecRecover
    
